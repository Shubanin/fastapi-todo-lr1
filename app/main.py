"""Main of todo app
"""
from loguru import logger

from fastapi import FastAPI, Request, Depends, Form, status, HTTPException, Query
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from database import init_db, get_db, Session
import models
import random
import string
from sqlalchemy.orm import Session
from database import SESSIONLOCAL
import models
init_db()

# pylint: disable=invalid-name
templates = Jinja2Templates(directory="templates")

app = FastAPI()

logger = logger.opt(colors=True)
# pylint: enable=invalid-name

app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/")
async def get_todos(
        request: Request,
        page: int = Query(1, ge=1),  # Start from the first page
        per_page: int = Query(10, le=100),  # Limit the number of todos per page
        database: Session = Depends(get_db)
):
    """Get paginated todos
    """
    logger.info("Fetching paginated todos")
    todos_query = database.query(models.Todo).order_by(models.Todo.id.desc())

    total_todos = todos_query.count()
    todos = todos_query.offset((page - 1) * per_page).limit(per_page).all()

    return templates.TemplateResponse("index.html", {
        "request": request,
        "todos": todos,
        "page": page,
        "per_page": per_page,
        "total_todos": total_todos
    })

@app.post("/add")
async def todo_add(request: Request, title: str = Form(default=None), details: str = Form("Описание"), database: Session = Depends(get_db)):
    """Add new todo
    """
    if title is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Title cannot be empty")

    if len(title) > 500:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Title length exceeds 500 characters limit")

    todo = models.Todo(title=title, details=details)
    logger.info(f"Creating todo: {todo}")
    database.add(todo)
    database.commit()
    return RedirectResponse(url=app.url_path_for("get_todos"), status_code=status.HTTP_303_SEE_OTHER)

@app.get("/edit/{todo_id}")
async def todo_get(request: Request, todo_id: int, database: Session = Depends(get_db)):
    """Get todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Todo not found")
    logger.info(f"Getting todo: {todo}")
    return templates.TemplateResponse("edit.html", {"request": request, "todo": todo})

@app.post("/edit/{todo_id}")
async def todo_edit(
        request: Request,
        todo_id: int,
        title: str = Form(default=None),
        completed: bool = Form(False),
        database: Session = Depends(get_db)):
    """Edit todo
    """
    logger.info(print("todo_id : ", todo_id))
    logger.info(print("title : ", title))
    logger.info(print("completed : ", completed))
    logger.info(print("todo_id : todo_id"))
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Todo not found")
    logger.info(f"Editing todo: {todo}")

    if title is not None:
        todo.title = title
    todo.completed = completed
    database.commit()
    return RedirectResponse(url=app.url_path_for("get_todos"), status_code=status.HTTP_303_SEE_OTHER)

@app.get("/delete/{todo_id}")
async def todo_delete(request: Request, todo_id: int, database: Session = Depends(get_db)):
    """Delete todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Todo not found")
    logger.info(f"Deleting todo: {todo}")
    database.delete(todo)
    database.commit()
    return RedirectResponse(url=app.url_path_for("get_todos"), status_code=status.HTTP_303_SEE_OTHER)
@app.get("/todos")
async def get_todos(
        request: Request,
        page: int = Query(1, ge=1),  # Начинать с первой страницы
        per_page: int = Query(10, le=100),  # Ограничить количество todo на странице
        database: Session = Depends(get_db)
):
    """Get paginated todos"""

    logger.info("Fetching paginated todos")
    todos_query = database.query(models.Todo).order_by(models.Todo.id.desc())

    total_todos = todos_query.count()

    # Рассчитываем общее количество страниц
    total_pages = (total_todos - 1) // per_page + 1

    # Проверяем, не превышает ли запрошенная страница общее количество страниц
    if page > total_pages:
        # Если превышает, перенаправляем на последнюю страницу
        return RedirectResponse(url=f"/todos?page={total_pages}&per_page={per_page}")

    todos = todos_query.offset((page - 1) * per_page).limit(per_page).all()

    return templates.TemplateResponse("index.html", {
        "request": request,
        "todos": todos,
        "page": page,
        "per_page": per_page,
        "total_todos": total_todos
    })

def generate_random_title(length):
    """генерирует случайную строку"""
    return ''.join(random.choices(string.ascii_letters + string.digits, k=length))

@app.post("/create_random_todo")
async def create_random_todo(database: Session, title_length = 10):
    """Создает случайную todo с случайным заголовком длмной в 10 символов"""
    title = generate_random_title(title_length)
    details = "Описание задачи"
    todo = models.Todo(title=title, details=details)
    database.add(todo)
    database.commit()

    # Инициализация соединения с базой данных
    db = SESSIONLOCAL()

    # Генерирация 20 случайных todo
    for _ in range(20):
        create_random_todo(db)

    # Закрываю соединение с базой данных
    db.close()