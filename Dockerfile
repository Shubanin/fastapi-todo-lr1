FROM python:3.10-slim

COPY requirements.txt /requirements.txt
RUN pip install --no-cache -r /requirements.txt

COPY app /app
WORKDIR /app

ENTRYPOINT ["uvicorn", "main:app", "--reload", "--host=0.0.0.0", "--port=8000"]
